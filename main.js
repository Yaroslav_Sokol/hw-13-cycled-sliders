// 1. SetTimeout дозволяє викликати функцію лише один раз через певний інтервал часу
//    SetInterval дозволяє викликати функцію регулярно, повторюючи виклик через певний інтервал часу
// 2. Якщо в функцію setTimeout передати нульов затримку то ця функція не спрацює миттєво, але буде додана в чергу виконання, 
//    яка буде виконане якомога швидше. Тому навіть якщо затримка буде 0 то функція не спрацює миттєво, а лише після того як 
//    стек виконання JS буде вільним.
// 3. Викликання цієї функції є дуже важлива, оскільки без неї цикл буде продовжуваться навіть якщо він більше не потрібний 
//    та може перешкоджувати нормальній роботі коду.

const images = document.querySelectorAll ('.image-to-show');
const stopButton = document.createElement('button');
const resumeButton = document.createElement('button');
const timer = document.createElement('div');

let currentIndex = 0;
let timerInterval;

function showCurrentImage() {
    images[currentIndex].style.opacity = 1;
    images[currentIndex].style.transition = 'opacity 0.5s ease-in-out';
    for (let i = 0; i < images.length; i++) {
      if (i !== currentIndex) {
        images[i].style.opacity = 0;
      }
    }
    currentIndex = (currentIndex + 1) % images.length;
  }
  
  function startTimer() {
    let timeLeft = 3000;
    timer.textContent = `Next image in ${(timeLeft / 1000).toFixed(1)}s`;
    timerInterval = setInterval(() => {
      timeLeft -= 10;
      timer.textContent = `Next image in ${(timeLeft / 1000).toFixed(1)}s`;
      if (timeLeft <= 0) {
        clearInterval(timerInterval);
        showCurrentImage();
        startTimer();
      }
    }, 10);
  }
  
  function stopTimer() {
    clearInterval(timerInterval);
  }
  
  function resumeTimer() {
    startTimer();
  }
  
  stopButton.textContent = 'Припинити';
  resumeButton.textContent = 'Відновити показ';
  
  document.body.appendChild(stopButton);
  document.body.appendChild(resumeButton);
  document.body.appendChild(timer);
  
  stopButton.addEventListener('click', stopTimer);
  resumeButton.addEventListener('click', resumeTimer);
  
  showCurrentImage();
  startTimer();